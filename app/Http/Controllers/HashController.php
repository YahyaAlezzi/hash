<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\log;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class HashController extends Controller
{
   public function hash ()
   {
       Log::info('hash');
       $hash=Hash::make(str_random(10));
       return response()->json(compact('hash'));
    }
   
}
